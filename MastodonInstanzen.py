#!/usr/bin/env python
import json
import requests
import math
from collections import OrderedDict


instances=[line.rstrip('\n') for line in open("instances-urls.txt")]


TEMPLATE="""
| Instanz | Admin | Block | Offen | Einladung | Version | User | Posts | Zeichen |
|-|-|-|-|-|-|-:|-:|-:|
"""

output = TEMPLATE

# Get the data from every instance in the instances-urls.txt file
result = {}
for instance in instances:
  try:
    req          = requests.get(instance + "/api/v1/instance")
    # req.add_header("User-agent","Mozilla/5.0 (X11; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0");
#   req.add_header("User-agent","Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0");
    #print("req.text: " +req.text)
    if 'application/json' in req.headers.get('Content-Type'):
      data         = json.loads(req.text)
      bool_reg     = data.get("registrations", False);
      bool_approv  = data.get("approval_required", False);
      str_version  = data["version"]
      num_users    = data["stats"]["user_count"]
      num_statuses = data["stats"]["status_count"]
      num_chars    = data.get("max_toot_chars",500)
      str_mail     = data.get("email", "-").replace(" [a] ", "@")
      str_admin    = data.get("contact_account", {}).get("url", "https://fehlt")
      str_name     = data.get("contact_account", {}).get("username", "")
      str_block    = instance + "/about/more#unavailable-content"


      result[instance]={
      "bool_reg":bool_reg,
      "bool_approv":bool_approv,
      "str_version":str_version[:3],
      "num_users":num_users,
      "num_statuses":num_statuses,
      "num_chars":num_chars,
      "str_mail":str_mail,
      "str_admin":str_admin,
      "str_name":str_name,
      "str_block":str_block}
  except requests.exceptions.RequestException as e:
    print("Error fetching data from: {instance}".format(instance=instance))

# Sort the result by str_version und num_users
sorted_result = OrderedDict(
    sorted(result.items(),
    key=lambda instance: (
        instance[1]["bool_reg"],
        not instance[1]["bool_approv"],
        instance[1]["str_version"],
        (instance[1]["num_statuses"] / instance[1]["num_users"]) * math.sqrt(instance[1]["num_users"])
    ), reverse=True))

# Output it as a table
for instance,value in sorted_result.items():
  output += "| [{instance2}]({instance}) | ([{str_name}]({str_admin})) | [🔇]({str_block}) | {bool_reg} | {bool_approv} | {str_version} | {num_users} | {num_statuses} | {num_chars} | \n".format(
            instance     = instance,
            instance2    = instance.replace("https://", ""),
            bool_reg     = "✅" if value["bool_reg"] else "⛔",
            bool_approv  = "⛔" if value["bool_approv"] else "✅",
            str_version  = value["str_version"],
            num_users    = value["num_users"],
            num_statuses = value["num_statuses"],
            num_chars    = value["num_chars"],
            str_mail     = value["str_mail"],
            str_admin    = value["str_admin"],
            str_name     = value["str_name"],
            str_block    = value["str_block"]
)

print(output)
